declare
   l_new_val varchar2(50);
begin
select next_stage_val into l_new_val from (select MAX( TO_NUMBER( CASE WHEN REGEXP_LIKE(SUBSTR(table_name,3,1),'[0-9]') THEN SUBSTR(table_name,2,2) ELSE SUBSTR(table_name,2,1) END))+ 1 NEXT_STAGE_VAL from user_tab_cols where SUBSTR(table_name,1,1)!='R' AND REGEXP_LIKE(table_name,'(^[A-Z]{1})([0-9]+\_)([\_A-Z])+') AND SUBSTR(table_name,1,1)='P');
execute immediate 'ALTER TABLE A_GROUPS RENAME TO P'|| l_new_val||'_GROUPS';
execute immediate 'ALTER TABLE A_GROUP_CONTACT RENAME TO P'|| l_new_val||'_GROUP_CONTACT';
execute immediate 'ALTER TABLE A_GROUP_ADDRESS RENAME TO P'|| l_new_val||'_GROUP_ADDRESS';
execute immediate 'ALTER TABLE A_GROUP_CUSTOMER_ACCOUNT RENAME TO P'|| l_new_val||'_GROUP_CUSTOMER_ACCOUNT';
execute immediate 'ALTER TABLE A_GROUP_PLAN RENAME TO P'|| l_new_val||'_GROUP_PLAN';
execute immediate 'ALTER TABLE A_GROUP_PLAN_POLICY_TYPE RENAME TO P'|| l_new_val||'_GROUP_PLAN_POLICY_TYPE';
execute immediate 'ALTER TABLE A_GROUP_POLICY_TYPE_PROD_OPT RENAME TO P'|| l_new_val||'_GROUP_POLICY_TYPE_PROD_OPT';
execute immediate 'ALTER TABLE A_GROUP_PLAN_ADVISORS RENAME TO P'|| l_new_val||'_GROUP_PLAN_ADVISORS';
end;
/

INSERT INTO script_executed (
    id,
    name,
    description,
    filename,
    created_date,
    executed_date
) VALUES (
    script_executed_s.NEXTVAL,
    'renewal-migration',
    'group renewal migration',
    'r-migration-script.sql',
    SYSDATE,
    SYSDATE
);
COMMIT;
QUIT;
